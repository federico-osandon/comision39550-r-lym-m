import { doc, getDoc, getFirestore } from "firebase/firestore"
import { useEffect, useState } from "react"

import { useParams } from "react-router-dom"
import { TextComponent, TextComponent2, TextComponent3, TextComponent4, TextComponent5, TextComponent6, TextComponent7 } from "../../clases/clase11/ComponenteEjemplosCondicionales"

import { gFetch } from "../../utils/gFecht"
import ItemDetail from "../IItemDetail/ItemDetail"


const ItemDetailConainer = () => {
    const [product, setProduct] = useState({})
    const [loading, setLoading] = useState(true)
    const { idProducto } = useParams()

    // acceder a un documento -> ItemDetailContainer
    // useParams
    useEffect(()=>{
        const db = getFirestore()
        const queryDoc = doc(db, 'Productos', idProducto)
        getDoc(queryDoc)
        .then(respProd => setProduct(  { id: respProd.id, ...respProd.data() }  ))
        .catch(err => console.error(err))
        .finally(()=> setLoading(false)) 
    },[])

    
    
    return (
        <>
            {/* <TextComponent >
            </TextComponent> */}
                {/* <TextComponent7  /> */}
                {loading ? 
                        <h2>Loading...</h2>
                    :
                        <ItemDetail product={product} />}
        </>
    )
}

export default ItemDetailConainer