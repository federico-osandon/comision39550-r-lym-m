import { Container, Nav, Navbar } from "react-bootstrap"
import { NavLink } from "react-router-dom"

import CartWidget from "../CartWidget/CartWidget"

const categorias = [
  {id: "jlajsdflsajdf", path: "/categoria/gorras", name:"Gorras", description:'Esto son las gorras' },
  {id: "jlajsdflsajdf1", path: "/categoria/remeras", name:"Remeras", description:'Esto son las gorras' },
  // {id: "jlajsdflsajdf2", path: "/categoria/pantalones", name:"Pantalón", description:'Esto son las gorras' }
]


// Componente Menu
function NavBar() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <NavLink to='/' className="btn btn-success">Fede EApp</NavLink>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            {categorias.map(cat =>  <NavLink key={cat.id} className={({isActive})=> isActive ? 'btn btn-primary' : 'btn btn-outline-primary' } to={cat.path}>{cat.name}</NavLink>)}            
            
          </Nav>
          <Nav>
            {/* <Nav.Link eventKey={2} href="#memes">
            </Nav.Link> */}
            <NavLink to='/cart'>
              <CartWidget />
            </NavLink>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavBar