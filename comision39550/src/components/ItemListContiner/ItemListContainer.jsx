import { useEffect, useState } from "react"
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore'
import { useParams } from "react-router-dom"

import ItemList from "../ItemList/ItemList"
import { Loading } from "../Loading/Loading"

export const ItemListContainer = ( {saludos}) => {
    const [productos, setProductos] = useState([])   
    const [loading, setLoading ] = useState(true)      
    const { idCategoria } = useParams()    
     
    /* A React Hook that is called when the component is mounted and when the idCategoria changes. */
    useEffect(()=>{
        const db = getFirestore()        
        const queryCollection = collection(db, 'Productos')
        
        const queryFilter = idCategoria ? query(queryCollection, where( 'categoria', '==' , idCategoria)) : queryCollection                

        getDocs(queryFilter)
        .then(respCollection => setProductos( respCollection.docs.map(prod => ({ id: prod.id, ...prod.data() })) ))
        .catch(err => console.error(err))
        .finally(()=> setLoading(false))         
    }, [idCategoria])
         
    return (
        <>
         
            { loading ?                    
                <Loading />
            : 
                <>                        
                    <h2>{saludos}</h2>
                    <ItemList productos={productos}/>
                </>
            }
        </>
    )
}

// Actualizar cód, simplificar cód, acomodar cód  =  refactorización
// eliminar cód repetido 
// prohibido el cód comentado 
// funciónes, cód que no usemos.  
// importaciones y variables declaradas nunca usadas, que no esté
// ningún console.log en nuestra app a menos lo de los errores. 
// no errores en la consola. 
// identar bien le cód
// separar bien lógica de componentes
// prohibido mezclar idiomas en el cód. 

// corroborar funcionamiento del cód 


