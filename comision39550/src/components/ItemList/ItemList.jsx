import { memo } from "react"
import Item from "../Item/Item"
import { styleCards } from "../ItemListContiner/ItemListContiner.style"

// memo() -> memo(componentes) -> memo(componente, fn(comparación))

const ItemList = memo(({ productos }) => {
    // console.log('ItemList')
    return (
        <div 
            className="justify-content-center align-item-center" 
            style={styleCards}
        >
            { 
                productos.map( producto =>  <Item producto={producto} /> ) 
            }
        </div>
    )
})

export default ItemList