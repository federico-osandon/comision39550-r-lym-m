import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {   

    const inputHandler = (event)=>{
        // event.preventDefault()
        // event.stopPropagation()
        if (['a','e','i','o','u'].includes(event.key)) {
            event.preventDefault()
            
        }
        console.log(event.key)     
    }
    // input.addEventListener('keydown', inputHandler)
    return (
        <div className="box w-100" >
            <div className="border border-5 border-warning w-50" >
                <input 
                    className="m-5" 
                    
                    onKeyDown={ inputHandler } 
                    // onClick={  inputHandler } 
                    type="text" 
                    name="nombre" 
                    id="i"
                />
            </div>
        </div>
    )
}
