import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'



function App() {

  const [count, setCount] = useState(0)

  // let i = 0
  // i = i + 1 // -> i++   //  i+= 1
  // i++
  // i+= 1 
  let condition = false
  // let resultado = ''
  // if (condition) {
  //   resultado = 'verdadero'
  // } else {
  //   resultado = 'falso'
    
  // }

  // console.log('El resultado es: ' + resultado + '. Federico')
  // ternario if else -> condtions ? :
  // console.log(`El resultado es: ${condition ? 'verdadero' : 'Falso' }. Federico`)


/// spread 
 
  // const array = [ 1, 2, 3 ]
  // const cuatro = 4
  // // push
  // const newArary  = [ cuatro, ...array ]
  // console.log(newArary)
  
  // propiedades dinámicas 
  let campo = 'lastName'

  const persona = {
    nombre: 'federico',
    email: 'f@gmail.com',
    [campo]: 'Osandon', 
    edad: 25
  }

  // destructing 
  // const nombre = persona.nombre
  // const email = persona.email

  const { nombre: lastName , email, edad= 35   } = persona


  console.log(edad)
  
  // function find(array, callback) {
  //   for (let i = 0; i < array.length; i++) {
  //     if (callback(array[i])) {
  //       return array[i];
  //     }
  //   }
  //   return undefined;
  // }

//   const numbers = [4, 9, 16, 25, 36];
// const firstSquare = find(numbers, n => n > 20);
// console.log(firstSquare)


//walter
// const array1 = [5, 12, 8, 130, 44];
  
// array1.forEach(function(numero) {
//   if (numero > 10){
//     console.log(numero);
//     // console.log('segundo')
    
//   }
// })
// Pero me devolvía todos los valores >10




  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Fede el mejor</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
