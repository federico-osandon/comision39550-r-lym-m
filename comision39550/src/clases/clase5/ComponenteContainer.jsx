import { useState, useEffect } from "react"
import Formulario from "../Formulario/Formulario"
import Titulo from "../Titulo/Titulo"

// Anatomía de un componente: 
      // Props de dos formas. 
      // estados
      // ciclo de vida: 
          // montaje -> es la primera que se renderiza el componente. Crea el nodo en el dom
          // actualización -> re render: una nueva llamada al componente.
              // Re render -> (nueva ejecución de mi componente) :
                // 1 - Cambio en los estados
                // 2 - Cambio en las props
                // 3 - Evento
          // desmontaje: elimina del dom 

      

      // hook
        //   [] <- useState

export const ComponenteContainer = ( {saludos, componente}) => {
    const [ count, setCount ] = useState(0)
    const [boleano, setBoleano] = useState(true)
    // console.log(props)
    // let count = 0 // estado de app 

    useEffect(()=>{
      // acciones 
      console.log(' Se produce simpre en cada render - 1 ')
    //   console.log('Simulación a una tarea pesada o async - Ej.: fetch("productos"), etc - 1 ')
      
    //   return ()=>{
    //     console.log('Efecto de limieza - window.removeEventListener()')
    //   }
    })

    useEffect(()=>{
      // acciones 
      console.log(' Una sola vez en el montaje - 2 ')
      console.log('Simulación a una tarea pesada o async - Ej.: fetch("productos"), etc - 2 ')
      
    //   return ()=>{
    //     console.log('Efecto de limieza - window.removeEventListener()')
    //   }
    }, [])

    useEffect(()=>{
      // acciones 
      console.log(' Solo cuando cambie boleano - 3 ')
    //   console.log('Simulación a una tarea pesada o async - Ej.: fetch("productos"), etc - 2 ')
      
    //   return ()=>{
    //     console.log('Efecto de limieza - window.removeEventListener()')
    //   }
    }, [ boleano ])
    
    let titulo = 'soy titulo de app'
    let fecha = Date()
    
    // llamadas a las apis fetch
    // console.log(estado)
    
    const hanldeCount = () => {
      // count = count + 1 //=> count ++ // count += 1
      // console.log(count)
      
      setCount(count + 1) // -> Cambio de estado
    }
    
    
    console.log('Renderind de componente container 4')
    return (
        <>
            <h1>Hola soy ComponenteContainer</h1>
            <h2>Contador: {count}</h2>
            <h2>Fecha de último Click: {fecha}</h2>
            {/* { componente } */}
            <button onClick={ () => hanldeCount }> Click </button>
            <button onClick={ () => setBoleano(!boleano) }> Boleano </button>
        </>
    )
}
