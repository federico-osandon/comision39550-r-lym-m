
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'

import NavBar from './components/NavBar/NavBar'
import { ItemListContainer } from './components/ItemListContiner/ItemListContainer'
import ItemDetailConainer from './components/ItemDetailContainer/ItemDetailConainer'
import CartContainer from './components/CartContainer/CartContainer'
import { CartContextProvider } from './context/CartContext'

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

function App() { // componente 
    // onAdd
    
    return (

        <div
            //className="border border-5 border-primary" 
            // onClick={()=>alert('Soy onClick de app')}
        >      
            <BrowserRouter>
                <CartContextProvider>
                    <NavBar />      
                    <Routes>
                        <Route path='/' element = { <ItemListContainer saludo='hola soy componente container'/> } />
                        <Route path='/categoria/:idCategoria' element = { <ItemListContainer saludo='hola soy componente container'/>} />
                        {/* <Route path='/categoria/remeras' element = {<ItemListContainer saludo='hola soy componente container'/>} />
                        <Route path='/categoria/pantolenes' element = {<ItemListContainer saludo='hola soy componente container'/>} /> */}
                        <Route path='/detalle/:idProducto' element = {<ItemDetailConainer />} />
                        <Route path='/cart' element = { <CartContainer />} />
                        {/* <Route path='/error' element = { <404NotFound />} /> */}

                        <Route path='*' element={ <Navigate to='/' />} />               
                    </Routes>
                    {/* <ItemCount inital={1} stock={10} onAdd={()=>{}} />      */}
                    {/* <Footer /> */}
                </CartContextProvider>    
            </BrowserRouter>


        </div>
    )
}

export default App
