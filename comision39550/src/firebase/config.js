
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCEmH-BDvL7Rxwj-vMCLR6ZIiNdljGORpo",
  authDomain: "comision39550.firebaseapp.com",
  projectId: "comision39550",
  storageBucket: "comision39550.appspot.com",
  messagingSenderId: "177446539635",
  appId: "1:177446539635:web:8abc48ac40435a24d2f506"
}

const app = initializeApp(firebaseConfig)

export const initFirebase = () => app